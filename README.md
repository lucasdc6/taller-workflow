# Taller workflow-continuo



En este taller se plantea implementar un repositorio de hooks de git para
validar el formato de commits, la implementación debe ser realizada aplicando
TDD, y de preferencia, validando ante cada push a master.

# Referencias

- [hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)
- [gitlab-ci](https://docs.gitlab.com/ee/ci/)
